set(APP_SRCS
  importOFTracks.cpp
  exportOFTracks.cpp
  visualizeMaxUnaries.cpp
  visualizeOpticalFlow.cpp
  visualizeOpticalFlowAsMagnitude.cpp
  mergeCamvid32ToCamvid11.cpp
  verifyLabelImages.cpp
  visualizeInconsistentTracks.cpp
  joinFeatures.cpp
  visualizeFeaturesXY.cpp
  visualizeFeaturesUV.cpp
  visualizeFeaturesXYUV.cpp
  visualizeDatasetUnaries.cpp
  visualizeTracks.cpp
  convertUncompressedEdgeMapToEXR.cpp
  )

foreach(SRC ${APP_SRCS})
  get_filename_component(BASENAME ${SRC} NAME_WE)
  message(STATUS "Adding ${BASENAME}")
  add_executable(${BASENAME} ${SRC})
  target_link_libraries(${BASENAME}
    IO
    Features
    Utils
  )
endforeach(SRC)