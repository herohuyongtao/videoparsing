#!/bin/bash

features_dir="."
out_dir="Results_WithFSO"

ConfigFile="DemoSeq.cfg"
Dataset="05VD"

start_frame=0
number_of_frames=100

## Hard coded blocksizes for demo. For real world scenes use adptive block size (See paper)
frames_per_block=50
block_step_size=25
block_fix_frame=30

EXEC=./runBlockVideoGMwFeatures

mkdir -p "$out_dir"
OutFileNames="$out_dir/${Dataset}_%06d.png"


feat_files=""
for (( i=0; i<=(number_of_frames-frames_per_block); i+=block_step_size )); do
  feat_start_frame=$((start_frame+i))
  feat_last_frame=$((feat_start_frame+frames_per_block-1))
  feat_fix_frame=$((feat_start_frame+block_fix_frame))
  feat_file="$(printf "$features_dir/VWFXY-${Dataset}-%06d-%06d-%06d.feat" $feat_start_frame $feat_last_frame $feat_fix_frame)"
  feat_files="$feat_files $feat_file"
done 

## Inference Params - This should be adjusted or learned based on the unaries used
INF_PARAMS="-i 10 --XYT.PottsWt 5 --XYT.sigmas 0.7 0.75 15 --XYTRGB.PottsWt 11 --XYTRGB.sigmas 12 14 40 5 5 5"
  

EXEC_ARGS="-d $ConfigFile $INF_PARAMS -s $start_frame -n $number_of_frames --display_save 2 --out_wildcard $OutFileNames -f $feat_files"
  
echo "Running $EXEC $EXEC_ARGS"
$EXEC $EXEC_ARGS

echo "Results saved in $out_dir"