#!/bin/bash

ConfigFile="DemoSeq.cfg"

## Hard coded blocksizes for demo. For real world scenes use adptive block size (See paper)
frames_per_block=50
block_step_size=25
block_fix_frame=30

dataset_start_frame=0
number_of_blocks=3


EXEC=./runFSO_XY


for (( block_id=0; block_id<number_of_blocks; block_id+=1 )); do
  start_frame=$((block_id*block_step_size + dataset_start_frame))
  fix_frame=$((start_frame+block_fix_frame))

  COMMAND="$EXEC -d $ConfigFile -s $start_frame -n $frames_per_block -f $fix_frame -v 0"

  echo "---------------------------- Block ID # $block_id / $number_of_blocks ------------------------------"
  echo "Running $COMMAND"
  $COMMAND
  echo "-----------------------------------------------------------------------------"
done