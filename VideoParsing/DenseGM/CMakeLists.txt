get_filename_component(MODULE_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)

file(GLOB _HDRS *.h *.hpp)
set(${MODULE_NAME}_HDRS ${_HDRS} PARENT_SCOPE)

file(GLOB MODULE_SRCS *.cpp)

########################### Add Target ################################

add_library(${MODULE_NAME} ${MODULE_SRCS} ${_HDRS})

target_include_directories(${MODULE_NAME}
  PUBLIC
    ${Boost_INCLUDE_DIRS}
    ${OpenCV_INCLUDE_DIR}
 )

target_include_directories(${MODULE_NAME}
  INTERFACE
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:${INSTALL_INCLUDE_DIR}>
 )

target_link_libraries(${MODULE_NAME}
    Core
    ${OpenCV_LIBS}
  )


######################### Installation Stuff ###########################

set_target_properties(${MODULE_NAME} PROPERTIES
  PUBLIC_HEADER "${_HDRS}"
  OUTPUT_NAME ${PROJECT_NAME}_${MODULE_NAME}
  )

# install(TARGETS ${MODULE_NAME}
#   EXPORT ${PROJECT_NAME}Targets
#   RUNTIME DESTINATION "${INSTALL_BIN_DIR}"
#   LIBRARY DESTINATION "${INSTALL_LIB_DIR}"
#   ARCHIVE DESTINATION "${INSTALL_LIB_DIR}"
#   PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}/${PROJECT_NAME}/${MODULE_NAME}" 
#   COMPONENT Devel
# )