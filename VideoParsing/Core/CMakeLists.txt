get_filename_component(MODULE_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)

file(GLOB _HDRS *.h *.hpp)
set(${MODULE_NAME}_HDRS ${_HDRS} PARENT_SCOPE)

########################### Add Target ################################

add_library(${MODULE_NAME} INTERFACE)

target_include_directories(${MODULE_NAME}
  INTERFACE
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:${INSTALL_INCLUDE_DIR}>
)

target_include_directories(${MODULE_NAME}
  INTERFACE
    ${EIGEN_INCLUDE_DIRS}
)

######################### Installation Stuff ###########################

# install(FILES ${_HDRS}
#   DESTINATION "${INSTALL_INCLUDE_DIR}/${PROJECT_NAME}/${MODULE_NAME}"
# )

# install(TARGETS ${MODULE_NAME}
#   EXPORT ${PROJECT_NAME}Targets
#   COMPONENT Devel
#   )