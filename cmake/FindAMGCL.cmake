# Try to find AMGCL http://ddemidov.github.io/amgcl/
# 
# You can simply use find_package(AMGCL) or find_package(AMGCL REQUIRED)
#
# Once done the following variables will be defined
#
#  AMGCL_FOUND              - True if AMGCL was found on your system
#  AMGCL_INCLUDE_DIRS       - List of directories of AMGCL and it's dependencies

find_path(AMGCL_INCLUDE_DIR 
  NAMES amgcl/amgcl.hpp
  PATHS ${CMAKE_INSTALL_PREFIX}/include
  )

set(AMGCL_INCLUDE_DIRS ${AMGCL_INCLUDE_DIR} )

#include the Standard package handler
include(FindPackageHandleStandardArgs)

# Handle QUIETLY and REQUIRED arguments and 
# set AMGCL_FOUND to TRUE if AMGCL_INCLUDE_DIR 
find_package_handle_standard_args(AMGCL DEFAULT_MSG AMGCL_INCLUDE_DIR)

mark_as_advanced(AMGCL_INCLUDE_DIR)

if(AMGCL_FOUND)
  add_library(AMGCL INTERFACE)
  target_include_directories(AMGCL 
    INTERFACE
      ${AMGCL_INCLUDE_DIRS}
  )
endif()